import random as rnd


# Добавить счетчик попыток за которые он может угадать число.
#
# На каждом шаге угадывания числа сообщайте пользователю сколько попыток у него осталось.
#
# Если пользователь не смог угадать за отведенное количество попыток сообщить ему об окончании (Looser!).
#
#
#
# Добавить сообщения-подсказки. Если пользователь ввел число, и не угадал - сообщать:
#
# "Холодно" если разница между загаданным и введенным числами больше 10, "Тепло" - если от 5 до 10 и "Горячо" если от 4 до 1.

def Hints(user: int, random_number: int, max_random_number: int) -> str:

    if 0 == user or user > max_random_number:
        return "Cold..."

    res = random_number - user

    if abs(res) > 10:
        return "Cold..."
    elif 5 <= abs(res) <= 10:
        return "Warm."
    elif 1 <= abs(res) <= 4:
        return "Hot!"
    #else:
        #return "Why?"

def simpleGame(random_number: int, max_random_number: int, attempts: int) -> str:
    wish = "y"
    local_attempts = attempts

    while wish == "y" and local_attempts != 0:
        user = input()
        try:
            user = int(user)
        except:
            return "Wrong Input!"
        if user == random_number:
            print("Congratulations! Do you want try again? (y/n)")
            wish = input().lower()
            if wish == "y":
                random_number = rnd.randint(1, max_random_number + 1)
                print(f"So, let's rerun. Generated from 1 to {max_random_number}...")
                local_attempts = attempts
                continue
            elif wish == "n":
                return "Good Bye!"
            else:
                return "Wrong input"
        else:
            local_attempts = local_attempts - 1
            #print("random_number =", random_number)
            if local_attempts != 0:
                print(Hints(user, random_number, max_random_number) + f" Attempts remaining {local_attempts}:")

    end_game = f"Random number was {random_number}. Haha Looser!"
    return end_game

def main():
    rnd.seed()
    attempts = 5
    max_random_number = 30
    random_number = rnd.randint(1, max_random_number + 1)
    print(f"Program generates a random int from 1 to {max_random_number}. Guess the number.")
    print("Enter whole integer number until you guess program random:")
    end_game = simpleGame(random_number, max_random_number, attempts)

    if end_game != "":
        print(end_game)
    else:
        print("The End")


main()
